
module.exports = {
    "cloak_status_code":true,
    "status_codes_to_pass_through":[401,400,404],
    "cloak_sensitive_headers":false,
    "headers_to_cloak":["Server","X-AspNet-Version","X-Powered-By"]
};

