module.exports = function( list ) {
    return list || [
        "app_profiles", 
        "bot_protection", 
        "request_limits", 
        "parameter_protection", 
        "url_protection", 
        "response_cloaking"
    ]
};