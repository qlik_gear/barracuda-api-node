module.exports = function(data) {
    return {
        "enabled": true,
        "type": data.type || "Input",
        "max_value_length": data.max_value_length || 1000,
        "parameter": data.parameter,
        "required": false,
        "ignore": false,
        "validate_parameter_name": true,
        "allowed_file_upload_types": "all",
        "parameter_type": data.parameter_type || "Input",
        "parameter_class": data.parameter_class ||  "Generic",
        "maximum_instances": "",
        "url_profile": data.url_profile,
        "base64_decode_parameter_value": true,
        "name": data.parameter,
        "block_cross_site_scripting": true,
        "block_directory_traversal": false,
        "block_os_command_injection": true,
        "block_remote_file_inclusion": false,
        "block_sql_injection": true,
        "file_upload_extensions": ["GIF", "JPG", "PDF"],
        "file_upload_mime_types": ["application/pdf", "image/gif", "image/jpeg"],
        "exception_patterns": ["perl-language-function-substrings"]
    }
}