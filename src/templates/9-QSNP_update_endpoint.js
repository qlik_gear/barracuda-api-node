module.exports = function(data) {
    return {
        "hostnames": data.hostnames,
        "redirectHTTP": "redirect",
        "serviceType": data.serviceType || "HTTPS",
        "servicePort": data.servicePort || "443",
        "automaticCertificate": false,
        "replaceCertificate": false,
        "session_timeout": 600,
        "keepaliveRequests": 64,
        "ntlmIgnoreExtraData": false,
        "enableVdi": false,
        "enableHttp2": false,
        "enableWebsocket": true,
        "enable_ssl_3": false,
        "enable_tls_1": false,
        "enable_tls_1_1": false,
        "enable_tls_1_2": true,
        "enable_tls_1_3": true,
        "enable_pfs": false,
        "cipher_suite_name": "all",
        "custom_ciphers": [],
        "redirectType": "temporary",
        "privateKey": data.privateKey,
        "certificate": data.certificate,
        "useOtherServiceIp": true
    };
}