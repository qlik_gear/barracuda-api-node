module.exports = {
	"enabled": true,
	"max_request_length": 32768,
	"max_request_line_length": 4096,
	"max_url_length": 4096,
	"max_query_length": 4096,
	"max_number_of_cookies": 40,
	"max_cookie_name_length": 64,
	"max_cookie_value_length": 4096,
	"max_number_of_headers": 30,
	"max_header_name_length": 1024,
	"max_header_value_length": 2000
};