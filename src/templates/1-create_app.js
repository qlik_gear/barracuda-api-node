module.exports = function(data) {

    return {
        // Websites (1)
        "applicationName": data.applicationName,
        "hostnames": [{
            "hostname": data.hostname // subdomain.domain.com
        }],

        // Endpoints (2)
        "useHttp": data.useHttp !== undefined? data.useHttp : true,
        "useHttps": data.useHttps !== undefined? data.useHttps : true,
        "httpServicePort": data.httpServicePort || "80",
        "httpsServicePort": data.httpsServicePort || "443",
        "redirectHTTP": data.redirectHTTP !== undefined? data.redirectHTTP : true,

        // Backend Server (3)
        "backendType": data.backendType || "HTTPS", //HTTP
        "backendIp": data.backendIp,
        "backendPort": data.backendPort,
        "useExistingIp": data.useExistingIp !== undefined? data.useExistingIp : true,

        // Select Mode (4)
        "maliciousTraffic": data.maliciousTraffic || "Active", //Passive

        "serviceType": data.serviceType || "HTTP"        
        
    }
}