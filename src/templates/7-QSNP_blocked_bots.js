module.exports = {
	"uncategorized": false,
	"site_monitor": false,
	"link_checker": false,
	"vulnerability_scanner": true,
	"speed_tester": false,
	"tool": false,
	"unrecognized": false,
	"search_engine_bot": false,
	"screenshot_creator": false,
	"web_scraper": false,
	"virus_scanner": false,
	"feed_fetcher": false,
	"marketing": false
};