module.exports = {
	"denied_metacharacters": "",
	"file_upload_extensions": ["GIF", "JPG", "PDF"],
	"python_php_attacks": "normal",
	"maximum_parameter_value_length": 11000,
	"directory_traversal": "none",
	"allowed_file_upload_types": "all",
	"ignore_parameters": ["reloadUri", "SAMLResponse", "__VIEWSTATE"],
	"file_upload_mime_types": ["application/pdf", "image/gif", "image/jpeg"],
	"exception_patterns": [],
	"apache_struts_attacks": "normal",
	"base64_decode_parameter_value": true,
	"custom_blocked_attack_types": [],
	"block_directory_traversal_strict": false,
	"validate_parameter_name": true,
	"cross_site_scripting": "normal",
	"ldap_injection": "normal",
	"maximum_instances": "",
	"enabled": true,
	"os_command_injection": "normal",
	"maximum_upload_file_size": 1024,
	"remote_file_inclusion": "none",
	"http_specific_injection": "normal",
	"sql_injection": "normal"
};