const axios = require('axios');
const BASE_URL = "https://api.waas.barracudanetworks.com/v2/waasapi/";

async function asyncForEach(array, cb) {
    for (let i = 0; i < array.length; i++) {
        await cb(array[i], i, array);
    }
}

class Waas {

    constructor(email, password) {
        if (!this._token || !this.token.key || this.token.expiry < Date.now() ) {
            console.log('No valid key found. Getting a new one.');
            this._auth(email, password).then(function(token) {
                this._token = token;
            }.bind(this));
        }     
    }

    async _auth(email, password) {
        let response = await axios({
            url: '/api_login/',
            baseURL: BASE_URL,
            method: 'POST',
            data: `email=${email}&password=${password}`
        })
        return response.data;
    }

    async _request(req) {
        while(!this._token) // define the condition as you like
            await new Promise(resolve => setTimeout(resolve, 1000));
  
        try {
            let headers = req.headers || {};
            headers['auth-api'] = this._token.key;  
            let response = await axios({
                url: `${req.path}`,
                baseURL: BASE_URL,
                method: req.method || 'POST',
                headers: headers,
                data: req.data
            });
            return response.data;
        } catch (error) {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(error.response.status, error.response.data);
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(error.request);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
            }
            console.log('Error Config URL:', error.config.url);
        }
    }

    async listApps() {
        return await this._request({ method: 'GET', path: '/applications/' });
    }

    async getAppDetails( appId ) {
        return await this._request({ method: 'GET', path: `/applications/${appId}/` });
    }

    async doneProvisioing( appId ) {
        return await this._request({method: 'GET', path: `/applications/${appId}/done_provisioning/`});
    }

    async getAppServers( appId ) {
        return await this._request({ method: 'GET', path: `/applications/${appId}/servers/` });
    }

    async getComponents( appId ) {
        return await this._request({ method: 'GET', path: `/applications/${appId}/components/` });
    }

    async getUrlProfiles( appId ) {
        return await this._request({ method: 'GET', path: `/applications/${appId}/url_profiles/` });
    }

    async addUrlProfile( appId, data ) {
        return await this._request({ method: 'POST', path: `/applications/${appId}/url_profiles/`, data: data });
    }

    async addUrlProfileParameter( appId, data ) {
        return await this._request({ method: 'POST', path: `/applications/${appId}/profile_parameter/`, data: data });
    }

    async updateUrlProfileParameter( appId, parameterId, data ) {
        return await this._request( { method: 'PATCH', path: `/applications/${appId}/profile_parameter/${parameterId}/`, data: data });
    }

    async enableComponents( appId, components ) {
        var all = await this._request({ method: 'GET', path: `/applications/${appId}/components/` });
        await asyncForEach(all.components, async function(comp)  {
            if ( components.indexOf(comp.sku) !== -1 && comp.is_available && !comp.is_added ) {
                console.log("Adding component: "+comp.sku);
                await this._request({ method: 'PATCH', path: `/applications/${appId}/components/${comp.id}/` })
            }
        }.bind(this));
    }

    async deleteApp( appId ) {
        return await this._request({ method: 'DELETE', path: `/applications/${appId}/` });
    }

    async createApp( data ) {
        return await this._request({
            method: 'POST',
            path: `/applications/`,
            data: data
        })
    }

    async changeBasicSecurity(appId, protectionMode){
        return await this._request({
            method: 'PATCH',
            path: `/applications/${appId}/basic_security/`,
            data: {
                "protection_mode": protectionMode // "Passive" or "Active"
            }
        })
    }

    async updateComponentUrlprotection(appId, patch) {
        return await this._request({
            method: 'PATCH',
            path: `/applications/${appId}/url_protection/`,
            data: patch
        })
    }

    async updateComponentRequestLimits(appId, patch) {
        return await this._request({
            method: 'PATCH',
            path: `/applications/${appId}/request_limits/`,
            data: patch
        })
    }

    async updateComponentParameterProtection(appId, patch) {
        return await this._request({
            method: 'PATCH',
            path: `/applications/${appId}/parameter_protection/`,
            data: patch
        })
    }

    async updateComponentBlockedBots(appId, patch) {
        return await this._request({
            method: 'PATCH',
            path: `/applications/${appId}/blocked_bots/`,
            data: patch
        })
    }

    async updateResponseCloaking(appId, patch) {
        return await this._request({
            method: 'PATCH',
            path: `/applications/${appId}/response_cloaking/`,
            data: patch
        })
    }

    async getAppAllEndpoints( appId ) {
        return await this._request({ method: 'GET', path: `/applications/${appId}/endpoints/` });
    }

    async getAppEndpoint( appId, endpointId ) {
        return await this._request({ method: 'GET', path: `/applications/${appId}/endpoints/${endpointId}/` });
    }
    
    async updateEndpoint(endpointId, patch) {
        return await this._request({
            method: 'PATCH',
            path: `/applications/0/endpoints/${endpointId}/`,
            data: patch
        });
    } 

    async changeAppRegion(appId, patch) {
        return await this._request({
            method: 'POST',
            path: `/applications/${appId}/regions/`,
            data: patch
        });
    }
}

module.exports = Waas;
