# Barracuda-api-node

## Pre-reqs

- NodeJS
- Yarn
- Git. Clone this project 
```
git clone https://gitlab.com/qlik_gear/barracuda-api-node.git
```

## Install

```shell
cd barracuda-api-node
yarn install 
```

## Test

Execute:

```shell

EMAIL='<email@email.com>' PASSWORD='<password>' node example.js

```