const QlikSenseNPrintingApp = require('./index').QlikSenseNPrintingApp;

/**
 * Example 1:
 * Qlik Sense App in Barracuda 
*/ 

const barracudaQSAppClient = new QlikSenseNPrintingApp();

barracudaQSAppClient.create(
    "exampleApp1", 
    "exampleApp1.qmi.qlik-poc.com", 
    "8.8.8.8", 
    "443"
).then(function(app){
    console.log("Newly created AppDetails", app);

    // Delete App 
    /*barracudaQSAppClient.delete(app.id).then(function(){
        console.log("Done delete app!");
    });*/

});


/** 
 * Example 2:
 * Raw access to WaasAPI
*/
const WaasAPI = require('./index').WaasAPI;
const waasClient = new WaasAPI(process.env.BARRACUDA_EMAIL, process.env.BARRACUDA_PASSWORD);
const appId = "10500";
waasClient.getAppDetails(appId).then(function(result){
    console.log("result", result);
})
