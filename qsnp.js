const WaasAPI = require('./src/api');
const certificates = require('./src/certs.json');


async function asyncForEach(array, cb) {
    for (let i = 0; i < array.length; i++) {
        await cb(array[i], i, array);
    }
}

class QlikSenseNPrintingApp {
    
    constructor() {
        this.bodyTemplates = {
            createApp: require('./src/templates/1-create_app'),
            componets: require('./src/templates/2-QSNP_add_components.js'),
            urlprofiles: require('./src/templates/3-QSNP_url_profiles.js'),
            urlprotection: require('./src/templates/4-QSNP_url_protection.js'),
            requestLimits: require('./src/templates/5-QSNP_request_limits.js'),
            parameterProtection: require('./src/templates/6-QSNP_parameter_protection.js'),
            blockedBots: require('./src/templates/7-QSNP_blocked_bots.js'),
            profileParameter: require('./src/templates/8-QSNP_profile_parameter.js'),
            endpoint: require('./src/templates/9-QSNP_update_endpoint.js'),
            cloaking: require('./src/templates/10-QSNP_response_cloaking.js'),

        };
    }

    get templates() {
        return this.bodyTemplates;
    }

    async create(appName, hostname, backendIp, backendPort, backendType) {

        const waasClient = new WaasAPI(process.env.BARRACUDA_EMAIL, process.env.BARRACUDA_PASSWORD);
    
        try {
    
            var newAppData = {
                applicationName: appName,
                hostname: hostname,
                backendIp: backendIp,
                backendPort: backendPort,
                httpsServicePort: backendPort,
                backendType: backendType || "HTTPS"
            };
    
            let app = await waasClient.createApp(this.bodyTemplates.createApp(newAppData));
            console.log("BarracudaAPI# New App", app);
    
            console.log("BarracudaAPI# Enabling components");
            await waasClient.enableComponents(app.id, this.bodyTemplates.componets());
            
            console.log("BarracudaAPI# Adding Url Profiles:");
            await asyncForEach(this.bodyTemplates.urlprofiles, async function(profile){
                console.log("BarracudaAPI# Adding UrlProfile: "+profile.url);
                let result = await waasClient.addUrlProfile(app.id, profile);
    
                if ( profile.url === "/qliksense/qrs/datacollection/settings" ) {
                    console.log("BarracudaAPI# Adding UrlProfile Parameter: xrfkey");
                    await waasClient.addUrlProfileParameter(app.id, this.bodyTemplates.profileParameter({
                        parameter: "xrfkey", 
                        url_profile: result.id
                    }));
                }
            }.bind(this));
    
            console.log("BarracudaAPI# Updating Component: Url Protection");
            await waasClient.updateComponentUrlprotection(app.id, this.bodyTemplates.urlprotection);
    
            console.log("BarracudaAPI# Updating Component: Request Limits");
            await waasClient.updateComponentRequestLimits(app.id, this.bodyTemplates.requestLimits);
    
            console.log("BarracudaAPI# Updating Component: Parameter Protection");
            await waasClient.updateComponentParameterProtection(app.id, this.bodyTemplates.parameterProtection);
    
            console.log("BarracudaAPI# Updating Component: Blocked Bots");
            await waasClient.updateComponentBlockedBots(app.id, this.bodyTemplates.blockedBots);

            console.log("BarracudaAPI# Updating Component: Reponse Cloaking");
            await waasClient.updateResponseCloaking(app.id, this.bodyTemplates.cloaking);
            
    
            console.log("BarracudaAPI# Updating HTTPS endpoint to set certificates");
            var endpoints = await waasClient.getAppAllEndpoints(app.id);     
            await asyncForEach(endpoints.results, async function(endpoint){
                if ( endpoint.protocol === "HTTPS") {
                    let info = {
                        hostnames: endpoint.dps_service.domains,
                        serviceType: endpoint.dps_service.service_type,
                        servicePort: endpoint.dps_service.port,
                        privateKey: certificates['qmi.qlik-poc.com'].privateKey,
                        certificate: certificates['qmi.qlik-poc.com'].certificate
                    }
                    await waasClient.updateEndpoint(endpoint.id, this.bodyTemplates.endpoint(info));
                }
            }.bind(this));


            if ( backendIp.includes("asia") ) {
                console.log("BarracudaAPI# Set region 'Singapore'");
                await waasClient.changeAppRegion(app.id, {
                    "region":"southeastasia",
                    "backup":"eastasia",
                    "is_auto_region":false,
                    "deploy_in_region":true,
                    "container":null,
                    "customer_container":false
                });
            } else {
                console.log("BarracudaAPI# Set region 'North America and West Europe'");
                await waasClient.changeAppRegion(app.id, {
                    "region":"anycast",
                    "is_auto_region":false
                });
            }
                
            
            let details = await waasClient.getAppDetails(app.id);
            let status = await waasClient.doneProvisioing(app.id);

            return {"app": details, "status": status};
        
            
        } catch(err) {
            console.log("err", err);
        }
    
    }

    async get(appId) {
        const waasClient = new WaasAPI(process.env.BARRACUDA_EMAIL, process.env.BARRACUDA_PASSWORD);
        return await waasClient.getAppDetails(appId);
    }

    async isProvisioned(appId) {
        const waasClient = new WaasAPI(process.env.BARRACUDA_EMAIL, process.env.BARRACUDA_PASSWORD);
        return await waasClient.doneProvisioing(appId);
    }

    async getDetails(appId){
        const waasClient = new WaasAPI(process.env.BARRACUDA_EMAIL, process.env.BARRACUDA_PASSWORD);

        let details = await waasClient.getAppDetails(appId);
        let status = await waasClient.doneProvisioing(appId);

        return {"app": details, "status": status};
    }

    async delete(appId) {
        const waasClient = new WaasAPI(process.env.BARRACUDA_EMAIL, process.env.BARRACUDA_PASSWORD);
        return await waasClient.deleteApp(appId);
    }

}

module.exports = QlikSenseNPrintingApp;